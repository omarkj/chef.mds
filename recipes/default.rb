#
# Cookbook Name:: mds
# Recipe:: default
#

include_recipe "mds::yum"

package "market_data_streamer" do
  if node['mds']['package_version'] != :latest then
    version  node['mds']['package_version']
  end
  action     :install
end

user "kodi" do
  gid       498
  action    :manage
end

directory "/etc/kodi/market_data_streamer" do
  action     :create
  recursive  true
  owner      "root"
  group      "root"
end

directory "/var/log/kodi/market_data_streamer" do
  action   :create
  recursive true
  owner     "kodi"
  group     "kodi"
end

template "/etc/kodi/market_data_streamer/app.config" do
  source  "app.config.erb"
  mode    0755
end

template "/etc/kodi/market_data_streamer/vm.args" do
  source  "vm.args.erb"
  mode    0755
end

template "/etc/init.d/market_data_streamer" do
  source  "mds.erb"
  mode 0755
end

service "market_data_streamer" do
  start_command "/etc/init.d/market_data_streamer start"
  stop_command "/etc/init.d/market_data_streamer stop"
  restart_command "/etc/init.d/market_data_streamer restart"
  status_command "/etc/init.d/market_data_streamer ping"
  supports :status => true, :restart => true
  action [ :enable, :start ]
end
