## Yum.rb

include_recipe "yum::yum"

## Add the Kodi repo

yum_repository "kodi-repo" do
  repo_name    "kodi-repo"
  description  "Kodi Repository"
  url          node['mds']['kodi_repo'] # Attribute
  action       :add
end
