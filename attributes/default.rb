# Default attribute file

default['mds']['kodi_repo'] = 'http://repo.livemarketdata.com/Kodi.Develop/$basearch'

default['mds']['package_version'] = :latest

default['mds']['handoff_port'] = '8090'
default['mds']['cluster'] = 'test'
default['mds']['cookie'] = 'develop'

default['mds']['user_system'] = 'http://localhost'
default['mds']['tip_http'] = 'http://localhost'

default['mds']['log_folder'] = '/var/log/kodi/market_data_streamer'
